// function isOdd(v) {
//   return v % == 1;
// }

// function isEven(v) {
//   return !isOdd(v);
// }

// isEven(4);

// **************** remove pass thru fcn

function isOdd(v) {
  return v % 2 == 1;
}

function not(fn) {
  return function negated(...args) {
    return !fn(...args);
  };
}

const isEven = not(isOdd);

console.log(isEven(6, 5));
// isEven(4);
