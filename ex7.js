// function foo() { return 42; }
// function bar() { return 73; }
function add(x, y) { return x + y; }
// let val1 = add(foo(), bar()); // 115
function add2(fn1, fn2) { return add( fn1(), fn2()); }
// let val2 = add2(foo, bar); // 115

function constant(value) {
  return () => value;
}

let val3 = add2(constant(42), constant(73));
console.log(val3);


// iterative
// function addN(fns) {
//   fns = fns.slice();
  
//   while (fns.length > 2) {
//     let [fn1, fn2, ...rest] = fns;
//     console.dir(fn1);
//     console.dir(fn2);
    
//     fns = [
//       () => add2(fn1, fn2),
//       ...rest
//     ]
    
//   }
//   return add2(fns[0], fns[1]);
// }

// recursive
// function addN([fn1, fn2, ...rest]) {
//   if (rest.length == 0) return add2(fn1, fn2)
//   return addN([() => add2(fn1, fn2), ...rest])
  
// }


// using reduce
function addN(fns) {
  return fns.reduce((acc, v) => {
      return () => add2(acc, v);
  })();
}

let val4 = addN([constant(1), constant(2), constant(3), constant(4)]);
console.log('val4', val4);
 

let val5 = [9, 5, 21, 26, 10, 20, 16, 2, 13, 27, 8, 6, 30]

.reduce((newList, v) => { // dedup
  if (newList.indexOf(v) == -1) return newList.concat(v); // concat vs push to not mutate data
  return newList;
}, [])
.filter((v) => v % 2 == 0) // only evens
.map((v) => () => (v) )


console.log('val5', val5);
