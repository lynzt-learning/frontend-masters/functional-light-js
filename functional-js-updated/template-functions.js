// // template fcn...
// let name = 'harry';
// let age = 'owl';





// greet`my name is ${name} and i have a pet ${age}.`;


// *********************
let wizards = [
  {name: 'harry', pet: 'owl'},
  {name: 'ron', pet: 'rat'},
  {name: 'hermione', pet: 'cat'},
];

function greet(strings, name, age) {
  console.log(strings[0] + name + strings[1] + age + strings[2]);
}

wizards.forEach(wizard => {
  // greet`My name is ${wizard.name} and i have a ${wizard.pet}.`;
  console.log(`My name is ${wizard.name} and i have a ${wizard.pet}.`);
});



const myFunction = () => {
  return 'Returned from myFunction!';
};

const templateResult = `Function expression in template: ${() => myFunction()}`;
console.log(templateResult); //Outputs -> Function expression in template: () => myFunction()

const myTag = (literals, func) => {
  return literals[0] + func();
};

const taggedResult = myTag `Function expression in template: ${() => myFunction()}`;
console.log(taggedResult); //Outputs -> Function expression in template: Returned from myFunction!
