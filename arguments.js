function spreadArgs(fn) {
  // return (args) => fn(...args);
  return function spread(args) {
    return fn(...args);
  }
}

function f(x, y, z, w) {
  console.log(x, y, z, w);
}

let g = spreadArgs(f);

g([1, 2, 3, 4]);
