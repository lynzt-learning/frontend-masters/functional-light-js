function add1(v) { return v + 1; }
function isOdd(v) { return v % 2 == 1; }
function sum(total, v) { return total + v; }

var list = [2, 5, 8, 11, 14, 17, 20];

// let val1 = list 
//   .map(add1)
//   .filter(isOdd)
//   .reduce(sum);
// console.dir(val1);


function listCombo(list, v) {
  list.push(v);
  return list;
}

// function mapReducer(mappingFcn) {
//   return (list, v) => {
//     return listCombo(list, mappingFcn(v));
//     // list.push( mappingFcn(v));
//     // return list;
//   };
// }

// function filterReducer(filterFcn) {
//   return (list, v) => {
//     if (filterFcn(v)) return listCombo(list, v);
//     // if (filterFcn(v)) list.push(v);
//     return list;
//   };
// }

let val2 = list
  .reduce(mapReducer(add1), [])
  .reduce(filterReducer(isOdd), [])
  .reduce(sum);
console.dir(val2);


var mapReducer = curry(function mapReducer(mappingFcn, combineFcn) {
  return (list, v) => combineFcn(list, mappingFcn(v));
});

var filterReducer = curry(function filterReducer(filterFcn, combineFcn) {
  return (list, v) => {
    if (filterFcn(v)) return combineFcn(list, v);
    return list;
  };
});

var val3 = list
  .reduce(mapReducer(add1)(listCombo), [])
  .reduce(filterReducer(isOdd)(listCombo), [])
  .reduce(sum);
