function add1(v) { return v + 1; }
function add10(v) { return v + 10; }
function mul2(v) { return v * 2; }
function div3(v) { return v / 3; }

var list = [2, 5, 8, 11, 14, 17, 20];

// inefficient - make new arr every map call (lots of space)
// have to loop arr multiple times
var val1 = list.map(add1)
    .map(mul2)
    .map(div3);
console.dir(val1);


// fusion - take series of list ops and compose together into single operator to run across list
function composeRight(fn1, fn2) {
  return (...args) => fn1(fn2(...args));
}

var val2 = list.map(
  [add10, div3, mul2, add1].reduce(composeRight)
);
console.dir(val2);
