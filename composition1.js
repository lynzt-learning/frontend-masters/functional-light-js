function sum (x, y) {
  return x + y;
}

function mult (x, y) {
  return x * y;
}

// (determine how??)
// function multAndSum(x, y, z) {
//   return sum(mult(x, y), z);
// }

// machine making machine...
function pipe2(fn1, fn2) {
  return function piped(arg1, arg2, arg3) {
    return fn2(
      fn1(arg1, arg2),
      arg3
    )
  }
  return fn2(
    fn1(arg1, arg2)
  )
}

let multAndSum = pipe2(mult, sum);

// *********** imparitive
let val1 = mult(3, 4);
let val2 = sum(val1, 5);
console.log(val2);

// *********** declaritive
let val3 = sum(mult(3,4), 5);
console.log(val3);


// (what - don't care how, only care what)
// let val4 = multAndSum(3, 4, 5);
// console.log(val4);



let val5 = multAndSum(3, 4, 5);
console.log(val5);



// *********** **
function composeRight(fn2, fn1) {
  return function comp(...args) {
    return fn2(fn1(...args));
  };
}

function inc(x) {
  return x + 1;
}

function double(x) {
  return x * 2;
}

let cr1 = composeRight(inc, double);
let cr2 = composeRight(double, inc);

console.log(cr1(3));
console.log(cr2(3));
