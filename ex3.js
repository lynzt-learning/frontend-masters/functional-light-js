function increment(x) { return x + 1; }
function decrement(x) { return x - 1; }
function double(x) { return x * 2; }
function half(x) { return x / 2; }

function compose(...fcns) {
  return pipe(...fcns.reverse());
}
function pipe(...fcns) {
  return function (value) {
    return fcns.reduce((accum, curr) => curr(accum), value);
  }
}

var f = compose(decrement,double,increment,half);
var p = pipe(half,increment,double,decrement);

f(3) === 4;
// console.log(f(3));
console.log(f(3));


// true

f(3) === p(3);
// true
