// impure
// function foo(x) {
//   y++;
//   z = x * y;
// }

// var y = 5, z;

// foo(20);
// console.log(z);

// foo(25);
// console.log(z);




function foo(x, y) {
  y++;
  console.log(y);
  
  z = x * y;
  return [y, z];
}

var y = 5, z;
[y, z] = foo(20, y);
console.log(z);

[y, z] = foo(25, y);
console.log(z);

