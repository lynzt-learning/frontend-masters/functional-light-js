const add3 = x => y => z => x + y + z;

var a1 = add3(3);
var a2 = a1(4);
var a3 = a2(5);
console.log(a3);

let a4 = add3(3)(4)(5);
console.log(a4);
